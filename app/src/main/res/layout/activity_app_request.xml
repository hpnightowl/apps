<?xml version="1.0" encoding="utf-8"?>

<!--
  ~ Copyright (C) 2019-2021  E FOUNDATION
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  -->

<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".settings.AppRequestActivity">

    <androidx.appcompat.widget.Toolbar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="?attr/actionBarSize"
        android:elevation="@dimen/default_elevation"
        android:theme="@style/ThemeOverlay.AppCompat.ActionBar"
        app:layout_constraintTop_toTopOf="parent"
        app:popupTheme="@style/ThemeOverlay.AppCompat.Light"
        app:title="@string/preference_apps_request_app_title" />

    <ScrollView
        android:id="@+id/scroll_view"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintTop_toBottomOf="@id/toolbar">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">

            <TextView
                android:id="@+id/app_request_error_text_view"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/layout_margin_large"
                android:layout_marginTop="32dp"
                android:layout_marginEnd="@dimen/layout_margin_large"
                android:background="@drawable/error_border"
                android:textAlignment="center"
                android:textColor="?android:textColorPrimary"
                android:textSize="@dimen/text_size_medium" />

            <EditText
                android:id="@+id/package_name_edit_text"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/layout_margin_large"
                android:layout_marginTop="32dp"
                android:layout_marginEnd="@dimen/layout_margin_large"
                android:layout_marginBottom="32dp"
                android:background="@drawable/edit_text_border"
                android:hint="@string/edit_text_hint"
                android:inputType="text"
                android:maxLines="1"
                android:padding="@dimen/layout_padding_large"
                app:layout_constraintTop_toBottomOf="@id/toolbar"
                android:importantForAutofill="no" />

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="horizontal">

                <View
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1" />

                <Button
                    android:id="@+id/submit_button"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:background="@drawable/app_install_border"
                    android:enabled="false"
                    android:foreground="?android:selectableItemBackground"
                    android:text="@string/submit_button_text"
                    android:textColor="@android:color/white"
                    app:layout_constraintTop_toBottomOf="@id/package_name_edit_text" />

                <View
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1" />

            </LinearLayout>

            <ProgressBar
                android:id="@+id/progress_bar"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="64dp" />

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/layout_margin_large"
                android:layout_marginTop="64dp"
                android:layout_marginEnd="@dimen/layout_margin_large"
                android:layout_marginBottom="@dimen/layout_margin_large"
                android:background="@drawable/app_install_border_simple"
                android:orientation="vertical">

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/layout_margin_large"
                    android:layout_marginTop="@dimen/layout_margin_large"
                    android:layout_marginEnd="@dimen/layout_margin_large"
                    android:layout_marginBottom="@dimen/layout_margin_medium"
                    android:text="@string/package_name_help_title"
                    android:textColor="?android:textColorPrimary"
                    android:textSize="@dimen/text_size_medium"
                    android:textStyle="bold" />

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/layout_margin_large"
                    android:layout_marginEnd="@dimen/layout_margin_large"
                    android:layout_marginBottom="@dimen/layout_margin_large"
                    android:text="@string/package_name_help_text"
                    android:textSize="@dimen/text_size_small" />

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/layout_margin_large"
                    android:layout_marginTop="@dimen/layout_margin_large"
                    android:layout_marginEnd="@dimen/layout_margin_large"
                    android:layout_marginBottom="@dimen/layout_margin_medium"
                    android:text="@string/package_name_find_title"
                    android:textColor="?android:textColorPrimary"
                    android:textSize="@dimen/text_size_medium"
                    android:textStyle="bold" />

                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/layout_margin_large"
                    android:layout_marginEnd="@dimen/layout_margin_large"
                    android:layout_marginBottom="@dimen/layout_margin_large"
                    android:text="@string/package_name_find_text"
                    android:textSize="@dimen/text_size_small" />

                <ImageView
                    android:layout_width="wrap_content"
                    android:layout_height="96dp"
                    android:layout_marginStart="@dimen/layout_margin_large"
                    android:layout_marginTop="@dimen/layout_margin_large"
                    android:layout_marginEnd="@dimen/layout_margin_large"
                    android:layout_marginBottom="@dimen/layout_margin_large"
                    android:adjustViewBounds="true"
                    android:src="@drawable/package_name_help_image"
                    android:contentDescription="@string/package_name_help_example" />

            </LinearLayout>

        </LinearLayout>

    </ScrollView>

</androidx.constraintlayout.widget.ConstraintLayout>